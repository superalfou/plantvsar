﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileControl : MonoBehaviour
{
    public Transform deviceTransform;
    public Transform target;
    public TrailRenderer trailRenderer;

    private bool insideTargetHitbox = false;

    // Start is called before the first frame update
    void Start()
    {
        Input.gyro.enabled = true;

#if !UNITY_EDITOR
        trailRenderer.emitting = false;
#endif
    }

    // Update is called once per frame
    void Update()
    {
#if !UNITY_EDITOR
            Controls();
#endif
    }

    private void Controls ()
    {
        Vector3 acceleration = Input.acceleration;

        if (acceleration.magnitude > 1.5f)
        {
            Debug.Log(acceleration.magnitude);

            trailRenderer.emitting = true;

            if (insideTargetHitbox)
            {
                Debug.Log("SWORD SLASH");
                SetTargetPosition();
            }
        }else
        {
            trailRenderer.emitting = false;
        }

    }


    public void SetTargetPosition ()
    {
        target.position = transform.position + (Vector3.forward * Random.Range(-1.2f, 1.2f)) + (Vector3.right * Random.Range(-1.2f, 1.2f));
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.transform.gameObject == target.gameObject)
        {
            Debug.Log("  >> inside target hitbox.");
            insideTargetHitbox = true;
        }

        if (other.tag == "target")
        {
            insideTargetHitbox = true;
            Debug.Log("  >>TAG inside target hitbox.");
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.transform.gameObject == target.gameObject)
        {
            Debug.Log("  << outside target hitbox.");
            insideTargetHitbox = false;
        }

        if (other.tag == "target")
        {
            insideTargetHitbox = false;
            Debug.Log("  <<TAG outside target hitbox.");
        }
    }
}
