﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "controller")
        {
            Debug.Log(" Target found controller inside");
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.tag == "controller")
        {
            Debug.Log("  Target found controller outside");
        }
    }
}
