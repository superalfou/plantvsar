﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drone : MonoBehaviour
{
    private void Update()
    {
        Roaming();
    }


    private float roamTargetTimer = 0f;
    private float roamTargetFrequency = 5f;
    private Vector3 roamTargetPosition;
    private void Roaming()
    {
        if (roamTargetTimer <= 0f)
        {
            SetRoamTarget();
        }
        roamTargetTimer -= Time.deltaTime;

        transform.position = Vector3.Lerp(transform.position, roamTargetPosition, Time.deltaTime * 0.5f);

    }

    private void SetRoamTarget()
    {
        Vector3 targetPosition = Camera.main.transform.position + (GetFlatHorizontalDirection(Camera.main.transform.forward) * 1.25f) + (Random.insideUnitSphere * Random.Range(0.25f, 1f));
        roamTargetPosition = targetPosition;
        Debug.Log("SetRoamTarget()");

        roamTargetTimer = roamTargetFrequency;
    }

    private Vector3 GetFlatHorizontalDirection(Vector3 dir)
    {
        return new Vector3(dir.x, 0f, dir.z);
    }
}
