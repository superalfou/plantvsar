﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;
using GoogleARCore.Examples.Common;

#if UNITY_EDITOR
//Enable touch propagation for Instant preview
using Input = GoogleARCore.InstantPreviewInput;
#endif

public class BallLauncher : MonoBehaviour
{
    [SerializeField]
    public GameObject m_ballPrefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UserInputs();
    }

    private void UserInputs ()
    {
        //Debug.Log("touch length= " + Input.touches.Length);

        if (Input.touches.Length > 0)
        {
            Touch touch = Input.touches[0];
            if (touch.phase == TouchPhase.Began)
            {
                GameObject ball = Instantiate(m_ballPrefab, Camera.main.transform.position, Quaternion.identity) as GameObject;
                ball.transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward);
                ball.GetComponent<Rigidbody>().AddForce(ball.transform.forward * 3f, ForceMode.Impulse);
            }

        }

        //Debug.Log(Input.touches.Length);

        

    }
}
