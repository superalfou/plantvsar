﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AVI_Barres : MonoBehaviour
{
    private Image foreground;

    public float max;
    public float startValue;

    private float valeur;
    // Start is called before the first frame update
    void Start()
    {
        foreground = transform.GetChild(0).GetComponent<Image>();
        valeur = startValue;
    }

    // Update is called once per frame
    void Update()
    {
        foreground.fillAmount = valeur/max;
    }

    public float Value {
        get { return valeur; }
        set { valeur = value; }
    }

}