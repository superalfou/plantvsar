﻿namespace GoogleARCore
{
    using GoogleARCoreInternal;
    using UnityEngine;
    using UnityEngine.Rendering;
    using UnityEngine.UI;

    public class AmbientLightChange : MonoBehaviour
    {
        public float lightIsDark;

        public GameObject lightScene;

        public Text dayOrNight;

        public void Update()
        {
            Debug.Log(Frame.LightEstimate.PixelIntensity);

            if(Frame.LightEstimate.PixelIntensity < lightIsDark)
            {
                lightScene.GetComponent<Light>().intensity = 0;
                dayOrNight.text = ("NIGHT");
            }
            else if (Frame.LightEstimate.PixelIntensity > 0.6)
            {
                lightScene.GetComponent<Light>().intensity = 1;
                dayOrNight.text = ("DAY");
            }
        }
    }
}
