﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;
using GoogleARCore.Examples.Common;

#if UNITY_EDITOR
//Enable touch propagation for Instant preview
using Input = GoogleARCore.InstantPreviewInput;
#endif



public class SpawnCafard : MonoBehaviour
{
    public GameObject cafardPrefab;

    bool cafardCanSpawn;

    void Start()
    {
        cafardCanSpawn = true;
    }
    void Update()
    {
        CafardSpawn();
    }



    void CafardSpawn()
    {
        if (cafardCanSpawn)
        {
            transform.position = new Vector3(transform.parent.position.x + Random.Range(-40f, 40f), transform.parent.position.y , transform.parent.position.z + Random.Range(-40f, 40f));
            GameObject cafard = Instantiate(cafardPrefab, transform.position, transform.rotation, transform.parent);

            StartCoroutine(CafardSpawnCoolDown());
        }
    }
    IEnumerator CafardSpawnCoolDown()
    {
        cafardCanSpawn = false;
        yield return new WaitForSeconds(2);
        cafardCanSpawn = true;
    }
}
