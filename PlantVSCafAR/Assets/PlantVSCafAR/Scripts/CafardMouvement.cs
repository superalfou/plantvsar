﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore;
using GoogleARCore.Examples.Common;

#if UNITY_EDITOR
//Enable touch propagation for Instant preview
using Input = GoogleARCore.InstantPreviewInput;
#endif




public class CafardMouvement : MonoBehaviour
{
    public float speed;


    void Start()
    {

    }
    void Update()
    {
        MoveToPlant();
    }


    void MoveToPlant()
    {
        Transform target = transform.parent; //Récupère le parent du cafard
        transform.position = Vector3.MoveTowards(transform.position, target.position, speed * Time.deltaTime); // Se dirige vers la plante
    }
}
